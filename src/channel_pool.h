/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Channel_Pool_h
#define _Channel_Pool_h 1

#include "channel.h"
#include "log_writer.h"

class ClickyServer;

class ChannelPool
{
public:
	ChannelPool( ClickyServer &_server, LogWriter *_logWriter = NULL ) : channelZero( _server, 0, _logWriter ) {};
	~ChannelPool() {};

	void SetLogWriter( LogWriter *_logWriter = NULL )
	{
		channelZero.SetLogWriter( _logWriter );
	};

	Channel *GetChannel( ChannelIdT id )
	{
		return (id == 0) ? &channelZero : NULL;
	};

protected:
	Channel channelZero;
};

#endif //_Channel_Pool_h
