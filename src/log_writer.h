/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Log_Writer_h
#define _Log_Writer_h 1

#include <cstdio>
#include "shared/src/clicky_time.h"
#include "shared/src/str_utils.h"
#include "shared/src/default_log.h"

class LogWriter
{
protected:
	std::string dir;
	std::string fileBase;
	size_t maxSize;
	size_t curSize;
	FILE *fileH;
	ClickyTime lastWriteTime;

public:
	static const size_t DEFAULT_MAX_LOG_SIZE = 100000;

	LogWriter( const std::string &_dir = ".",
			const std::string &_fileBase = "clicklog",
			size_t _maxSize = DEFAULT_MAX_LOG_SIZE )
	:	dir( _dir ),
		fileBase( _fileBase ),
		maxSize( _maxSize ),
		curSize(0),
		fileH(NULL),
		lastWriteTime(true)
	{};

	~LogWriter() {
		//Close();
	};

	static void FormatLogLine( const ClickyTime &logTime, const std::string &msg, std::string &dst )
	{
		std::string tmp = printLogTime( logTime );
		tmp.push_back('\t');
		tmp.append( msg );
		StrUtils::TrimInPlace( tmp );
		tmp.push_back('\n');
		dst.append( tmp );
	};

	void Write( const ClickyTime &logTime, const std::string &msg )
	{
		std::string logStr;
		const ClickyTime *lgT;
		if( logTime.IsValid() && ( logTime >= lastWriteTime ) ) {
			lgT = &logTime;
			lastWriteTime = logTime;
		} else {
			lgT = &lastWriteTime;
		}

		FormatLogLine( *lgT, msg, logStr );
		size_t logSize = logStr.size();

		LOG_FLG_DETAIL("Writing to log file: curSize = %ld, logSize = %ld, maxSize = %ld", curSize, logSize, maxSize );

		size_t res;
		if(( fileH == NULL )||( (curSize + logSize) > maxSize )) {
			if( !openNew( *lgT ) ) {
				LOG_FLG_ERROR("Failed to open a new log file\n");
				return;
			};
			std::string logFileIntro;
			onNewLogFile( *lgT, logFileIntro );
			logFileIntro.append( logStr );
			logSize = logFileIntro.size();
			res = fwrite( (void*)logFileIntro.c_str(), logSize, 1, fileH );
		} else {
			res = fwrite( (void*)logStr.c_str(), logSize, 1, fileH );
		}

		if( res != 1 ) {
			LOG_FLG_ERROR("Failed to write to log file. fwrite() returned %ld.\n", res );
			return;
		}

		fflush( fileH );
		curSize += logSize;
	};

	size_t Size() { return curSize; };

	void Close()
	{
		if( fileH == NULL ) { return; }
		fclose( fileH );
		fileH = NULL;
		curSize = 0;
	};

protected:

	/** Produces a string which is used as the introduction of a newly created log file.
	 * @param logTime - Use this as the date stamp of all log lines in the returned string.
	 * @param dst - Write the log lines to this destination string.
	 *
	 * Every line of the written string must be a valid log line
	 * and it must be dated logTime. Using any other date can disrupt
	 * continuity of the log file!
	 * The only alternative output is "", i.e., not touching dst at all.
	 */
	virtual void onNewLogFile( const ClickyTime &logTime, std::string &dst ) {};

	bool openNew( const ClickyTime &logTime )
	{
		if( fileH != NULL ) { Close(); }
		std::string logpath = makeLogPath( logTime );
		fileH = fopen( logpath.c_str(), "w" );
		curSize = 0;
		if( fileH == NULL ) {
			LOG_FLG_ERROR("Failed to open a new log file at \"%s\".\n", logpath.c_str() );
			return false;
		}
		LOG_FLG_INFO("Successfully opened a new log file at \"%s\".\n", logpath.c_str() );
		return true;
	};

	/** Produces something like log-20160831T125402.txt
	 */
	std::string makeLogPath( const ClickyTime &logTime )
	{
		std::string path = dir;
		std::string name = fileBase;
		name.push_back('-');
		name.append( logTime.ToDenseString( false ) );
		name.append(".txt");
		StrUtils::PathAppend(path,name);
		return path;
	};

	static std::string printLogTime( const ClickyTime &ct )
	{
		return ct.IsValid() ? ct.ToDenseString( false, 0 ) : "00000000000000";
	};
};

#endif //_Log_Writer_h
