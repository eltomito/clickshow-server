/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "log_guest_map.h"

void LogGuestMap::Add( ServerConPtr who )
{
	LOCK_ME;
	guestMap.insert( mapValT( who->GetId(), who ) );
}

void LogGuestMap::Rem( NodeConIdT whoId )
{
	LOCK_ME;
	guestMap.erase( whoId );
}
	
void LogGuestMap::ForEach( NCPForEachF (&doWhat)(ServerConPtr cl, void *_data), void *data )
{
	LOCK_ME;
	mapIt it = guestMap.begin();
	mapIt killit;
	NCPForEachF res;
	while( it != guestMap.end() ) {
		LOG_NET_DETAIL2("Iterating through guests: %s...\n", (*it).second->ToString().c_str() );
		res = doWhat((*it).second, data);
		if( res & NCP_DELETE ) {
			killit = it;
			++it;
			guestMap.erase( killit );
		} else {
			++it;
		}
		if( res & NCP_STOP ) { break; }
	};
}
