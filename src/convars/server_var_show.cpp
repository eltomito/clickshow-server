/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "server_var_show.h"
#include "../clicky_server.h"

#define SERVER_PTR ((ClickyServer *)(con.GetNode()))

/** Sends a SHOW packet to the connection this object belongs to.
 * @param num - subtitle number
 * @param text - subtitle text
 * @param clickTime - the time when this subtitle was clicked.
 */
void ServerVarShow::Send( int num, const std::string &text, const ClickyTime *clickTime )
{
	UdpPacketShow pkt( num, text, GetLastNumSent() );
	pkt.IncNum();
	if( clickTime != NULL ) {
		pkt.SetOrigTime( clickTime );
		con.Send( pkt );
	} else {
		con.Send( pkt );
	}
	JustSent( pkt );
};

void ServerVarShow::receive( UdpPacket &pkt, SPQActionT act, NodeUser *u )
{
	if( act == PROCESS ) {
		UdpPacketShow *p = (UdpPacketShow *)&pkt;
		if( !pkt.IsAck() ){
			SERVER_PTR->ClickSubtitle( *p, ((ServerCon&)con).GetSharedSelf(), curInSubNum );
			curInSubNum = p->subNum;
		} else {
			SERVER_PTR->LogShowAck( *p, ((ServerCon&)con).GetSharedSelf(), pkt.GetSentTime() );
		}
	}
}

void ServerVarShow::errorPerms()
{
	((ServerCon *)&con)->SendMsg( E_NO_RIGHT_TO_CLICK );
}
