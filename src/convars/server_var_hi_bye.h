/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Server_Var_Hi_Bye_h
#define _Server_Var_Hi_Bye_h 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/packet_queue_mtop.h"

class ServerVarHiBye : public GenConVar<MTOPQueue>
{
public:
	ServerVarHiBye(	NodeCon &_con, long _resendMilli = 500 );

	ServerVarHiBye(	NodeCon &_con, const ServerVarHiBye &other )
		: GenConVar<MTOPQueue>( _con, other ) {};
	~ServerVarHiBye() {};

	void SendBye();
	void receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u );
private:
	void afterHi();
};

#endif //_Server_Var_Hi_Bye_h
