/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "server_var_file_in.h"
#include "../server_con.h"
#include "../clicky_server.h"	

void ServerVarFileIn::FileStarted()
{
	LOG_NET_INFO("STARTED receiving file id %ld, file role %ld, size: %ld\n", GetFileId(), GetFileRole(), GetTotalSize() );
}

void ServerVarFileIn::FileProgressed()
{
	LOG_NET_DETAIL("PROGRESS on file id %ld, file role %ld, size: %ld / %ld\n", GetFileId(), GetFileRole(), GetCurrentSize(), GetTotalSize() );
}

void ServerVarFileIn::FileFinished()
{
	LOG_NET_INFO("FINISHED receiving file id %ld, file role %ld, size: %ld\n", GetFileId(), GetFileRole(), GetTotalSize() );
	if( GetFileRole() == FILE_ROLE_SUBTITLES ) {
		std::string dst;
		if( FileToString( dst ) ) {
			( (ClickyServer *)(con.GetNode()) )->SetSubtitles( dst, ((ServerCon&)con).GetSharedSelf() );
		}
	};
}

void ServerVarFileIn::FileAborted()
{
	LOG_NET_INFO("ABORTED receiving file id %ld\n", GetFileId() );
}
