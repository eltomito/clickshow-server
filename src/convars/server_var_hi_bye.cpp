/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "server_var_show.h"
#include "../clicky_server.h"

#include "shared/src/default_log.h"

#define SERVER_PTR ((ClickyServer *)(con.GetNode()))

ServerVarHiBye::ServerVarHiBye(	NodeCon &_con, long _resendMilli )
	: GenConVar<MTOPQueue>( _con, AUTO_RESEND, 0, _resendMilli )
{
	PacketTypeT tp[] = { HI, BYE, INVALID };
	GetQueue().SetTypes( tp );
};

void ServerVarHiBye::SendBye()
{
	UdpPacketBye pkt( GetLastNumSent() );
	pkt.IncNum();
	Send( pkt );
	JustSent( pkt );
};

void ServerVarHiBye::afterHi()
{
	//send the new Con(nection) its perms
	NodeUser *u = con.GetUser();
	if( u == NULL ) {
		LOG_NET_ERROR("Time to panic! This con is logged in but its user is still NULL! %s\n", con.ToString(",").c_str() );
		con.Kill( WHY_LOGOUT_NULL_USER );
	} else {
		((ServerCon *)&con)->SendPerms( u->GetPerms() );
	}
}

void ServerVarHiBye::receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u )
{
	if( pkt.type == HI ) {
	//hi
		if( act == REACK ) {
			LOG_NET_INFO("Already logged in! Reacking Hi...\n");
			pkt.SetAck();
			Send( pkt );
			JustSent( pkt );
			afterHi();
		}
		if( act != PROCESS ) { return; }
		UdpPacketHi *hip = (UdpPacketHi *)&pkt;
		LOG_NET_INFO("Logging in some con(nection)...\n");
		con.SetVenue( hip->clientId );
		if( SERVER_PTR->Login( ((ServerCon&)con).GetSharedSelf(), hip->loginName, hip->loginPwd ) ) {
			con.SetConnected( CONNECTED );
			LOG_NET_INFO("Logged in! Acking Hi: %s\n", con.ToString(",").c_str() );
			pkt.SetAck();
			Send( pkt );
			JustSent( pkt );
			afterHi();
		} else {
			LOG_NET_ERROR("Log in failed :( pkt:\n%s\n", pkt.ToString().c_str());
			con.Kill( WHY_LOGOUT_FAILED_LOGIN );
		}
	return;
	}
	//bye
	if( u == NULL ) {
		LOG_NET_WARN("A null user sent me Bye! %s.\n", pkt.ToString(",").c_str() );
		return;
	}
	if( pkt.IsAck() ) {
		if( con.IsDisconnecting() ) {
			LOG_NET_INFO("This connection ACKed my BYE: %s.\n", con.ToString(",").c_str() );
			con.Kill( WHY_LOGOUT_SERVER_BYE );
			return;
		} else {
			LOG_NET_ERROR("A connection acknowledged my bye, even thou it isn't disconnecting! Ignoring it.\n");
			return;
		}
	}
	if( act == PROCESS ) {
		LOG_NET_INFO("The connection disconnected from me. Acking its BYE: %s.\n", con.ToString(",").c_str() );
		pkt.SetAck();
		Send( pkt );
		JustSent( pkt );
		con.Kill( WHY_LOGOUT_CLIENT_BYE );
		return;
	}
	return;
};
