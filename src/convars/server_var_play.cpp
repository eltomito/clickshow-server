/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "server_var_play.h"
#include "../clicky_server.h"

#define SERVER_PTR ((ClickyServer *)(con.GetNode()))

/** Sends a PLAY packet to the connection this object belongs to.
 */
//void ServerVarPlay::Send( const ClickyTime &startTimestamp, double startTimecode, double delay, bool isPlaying )
void ServerVarPlay::Send( const PlayerState &ps )
{
	UdpPacketPlay pkt( ps, GetLastNumSent() );
	pkt.IncNum();
	con.Send( pkt );
	JustSent( pkt );
};

void ServerVarPlay::receive( UdpPacket &pkt, SPQActionT act, NodeUser *u )
{
	if( act == PROCESS ) {
		UdpPacketPlay *p = (UdpPacketPlay *)&pkt;
		if( !pkt.IsAck() ){
			SERVER_PTR->SetPlayerState( *p, ((ServerCon&)con).GetSharedSelf() );
		} else {
			//SERVER_PTR->LogPlayerStateAck( *p, ((ServerCon&)con).GetSharedSelf(), pkt.GetSentTime() );
		}
	}
}

void ServerVarPlay::errorPerms()
{
	((ServerCon *)&con)->SendMsg( E_NO_RIGHT_TO_CLICK );
}
