/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Server_Var_Play_h_
#define _Server_Var_Play_h_ 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/packet_queue_otop.h"
#include "shared/src/player/player_state.h"

class ServerVarPlay : public GenConVar<OTOPQueue<UdpPacketPlay> >
{
public:
	ServerVarPlay(	NodeCon &_con, long _resendMilli = 500 )
		: GenConVar<OTOPQueue<UdpPacketPlay> >( _con, NORMAL|PROCESS_ACK, PERM_CLICK_SUBS, _resendMilli ) {};

	ServerVarPlay(	NodeCon &_con, const ServerVarPlay &other )
		: GenConVar<OTOPQueue<UdpPacketPlay> >( _con, other ) {};

	~ServerVarPlay() {};

	void Send( const PlayerState &ps );
//	void Send( const ClickyTime &startTimestamp, double startTimecode, double delay, bool isPlaying );
	void receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u );
	void errorPerms();
};

#endif //_Server_Var_Play_h_
