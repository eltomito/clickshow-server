/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Server_Var_Perms_h
#define _Server_Var_Perms_h 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/packet_queue_otop.h"

class ServerVarPerms : public GenConVar<OTOPQueue<UdpPacketPerms> >
{
public:
	ServerVarPerms(	NodeCon &_con, long _resendMilli = 500 )
		: GenConVar<OTOPQueue<UdpPacketPerms> >( _con, NORMAL, PERM_SET_PERMS, _resendMilli ) {};
	ServerVarPerms(	NodeCon &_con, const ServerVarPerms &other ) : GenConVar<OTOPQueue<UdpPacketPerms> >( _con, other ) {};
	~ServerVarPerms() {};

	void Send( UserPermsT perms );
	void Receive( UdpPacketPerms &pkt );
};

#endif //_Server_Var_Perms_h
