/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Server_Var_File_In_h
#define _Server_Var_File_In_h 1

#include "shared/src/net/node/convars/con_var_file_in.h"

class ServerVarFileIn : public ConVarFileIn
{
public:
	ServerVarFileIn(	NodeCon &_con,
							netFileRoleT _fileRole = FILE_ROLE_INVALID,
							long _resendMilli = 500
						)	: ConVarFileIn(	_con,
													_fileRole,
													_resendMilli ) {};
	~ServerVarFileIn() {};

	virtual void FileStarted();
	virtual void FileProgressed();
	virtual void FileFinished();
	virtual void FileAborted();
};

#endif //_Server_Var_File_In_h
