/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "server_var_file_out.h"
#include "../server_con.h"
#include "../clicky_server.h"	
void ServerVarFileOut::FileStarted()
{
	LOG_NET_INFO("STARTED sending file id %ld, file role %ld, size: %ld\n", GetFileId(), GetFileRole(), GetTotalSize() );
}

void ServerVarFileOut::FileProgressed()
{
	LOG_NET_DETAIL("PROGRESS on file id %ld, file role %ld, size: %ld / %ld\n", GetFileId(), GetFileRole(), GetAckedSize(), GetTotalSize() );
}

void ServerVarFileOut::FileFinished()
{
	LOG_NET_INFO("FINISHED sending file id %ld, file role %ld, size: %ld\n", GetFileId(), GetFileRole(), GetTotalSize() );
	if( GetFileRole() == FILE_ROLE_SUBTITLES ) {
		((ServerCon*)&con )->UpdateShownSubtitle();
		((ClickyServer*)con.GetNode())->LogSendSubsAck( ((ServerCon&)con).GetSharedSelf(), GetTotalSize() );
	};
}

void ServerVarFileOut::FileAborted()
{
	LOG_NET_INFO("ABORTED sending file id %ld\n", GetFileId() );
}
