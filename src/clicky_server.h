/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Server_h
#define _Clicky_Server_h

#include "shared/src/net/node/clicky_node.h"
#include "server_con.h"
#include "channel_pool.h"
#include "shared/src/net/net_constants.h"

// ========== class ServerConfig ==============

#include "3rd/jsoncpp/dist/json/json.h"

class ClickyServer;

class ServerConfig
{
public:
	std::string		logFilePath;
	int				logFileSize;

public:
	ServerConfig( ClickyServer &_server ) : server(_server) { setDefaults(); };
	~ServerConfig() {};

	bool Read( const std::string & fileName );

protected:
	void setDefaults();
	bool readUsers( Json::Value &users );

protected:
	ClickyServer	&server;
};

//========== class ClickyServer ==============


using boost::asio::ip::udp;

class ClickyServer : public ClickyNode<ServerCon>
{
friend class ServerConfig;
public:
	ClickyServer(	boost::asio::io_service &_ioService,
						const std::string cfgFilePath = "clicky_server.cfg",
						unsigned short port = DEFAULT_SERVER_PORT )
		:	ClickyNode( _ioService, port, false ),
			channels( *this )
	{
		ServerConfig cfg( *this );
		if( !cfg.Read( cfgFilePath ) ) {
			LOG_NET_ERROR("FAILED to read the config! The server is probably useless!\n");
		};

		logWriter = new ChannelLogWriter( *channels.GetChannel(0), "", cfg.logFilePath, cfg.logFileSize );
		if( logWriter != NULL ) {
			LOG_GEN_INFO("LogWriter created!\n");
			channels.SetLogWriter( logWriter );
		} else {
			LOG_GEN_ERROR("Failed to create LogWriter!\n");
		}
	};

	~ClickyServer()
	{
		if( logWriter != NULL ) {
			delete logWriter;
			logWriter = NULL;
		}
	};

	void LogoutAll();

	using ClickyNode<ServerCon>::Start;
	using ClickyNode<ServerCon>::Stop;
	using ClickyNode<ServerCon>::Send;

	void ClickSubtitle( const UdpPacketShow &insub, ServerConPtr author, int believedLastSubNum );
	void SendShowSubtitle( Channel &ch );

	void SetPlayerState( const UdpPacketPlay &pkt, ServerConPtr author );
	void SendPlayerState( Channel &ch );

	void SetSubtitles( const std::string &subs, ServerConPtr author );
	void SendSetSubtitles( Channel &ch );

	void LogShowAck( const UdpPacketShow &pkt, const ServerConPtr con, const ClickyTime &when );
	void LogSendSubsAck( const ServerConPtr con, size_t size );

	Channel *GetChannelById( ChannelIdT id ) { return channels.GetChannel( id ); };

protected:
	void onLogin( ServerConPtr cl );
	void onLogout( ServerConPtr cl, NodeLogoutReasonT why );
	void onNewConnection( ConPtrT cl ) { cl->SetWeakSelf( boost::weak_ptr<ServerCon>( cl ) ); };
private:

	LogWriter	*logWriter;
	ChannelPool	channels;
};

#endif //_Clicky_Server_h
