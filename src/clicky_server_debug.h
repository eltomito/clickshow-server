/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _CLICKY_SERVER_DEBUG_H_
#define _CLICKY_SERVER_DEBUG_H_ 1

#ifdef DEBUG
#define LOG_LEVEL 4
#define LOG_MASK (LOGMOD_NET|LOGMOD_FILE|LOGMOD_FLG|LOGMOD_GEN)
#else
#define LOG_LEVEL 0
#define LOG_MASK (LOGMOD_ALL ^ LOGMOD_LOCKME)
#endif

#include "shared/src/default_log.h"

#endif //_CLICKY_SERVER_DEBUG_H_
