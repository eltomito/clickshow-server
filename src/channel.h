/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Channel_h
#define _Channel_h 1

#include "shared/src/net/packets/udp_packet_show.h"
#include "shared/src/net/packets/udp_packet_play.h"
#include "shared/src/net/net_types.h"
#include "shared/src/player/player_state.h"

class ChanSub
{
public:
	ChanSub()
	:	clickTime( true ),
		subNum( -2 ),
		subText(""),
		clickerId( 0 ) {};
	~ChanSub() {};

	void Set( const UdpPacketShow &pkt, NodeConIdT _authorId );
	void Reset( NodeConIdT _authorId = 0 ) { subNum = -2; subText = ""; clickerId = _authorId; clickTime.SetToNow(); }
	NodeConIdT GetClickerId() { return clickerId; };
	bool IsShown() { return subNum > -2; };

	int				subNum;
	std::string		subText;
	NodeConIdT		clickerId;
	ClickyTime		clickTime;
};

class ChanPlayerState : public PlayerState
{
public:
	ChanPlayerState() : PlayerState(),
			authorId( 0 ),	changeTime(true) {};
	~ChanPlayerState() {};

	void Set( const UdpPacketPlay &pkt, NodeConIdT _authorId );
	NodeConIdT GetAuthorId() { return authorId; };
	const ClickyTime &GetChangeTime() { return changeTime; };
	void SetSubNum( int _shownSub );

protected:
	NodeConIdT	authorId;
	ClickyTime	changeTime;
};

#include "channel_log.h"

class ClickyServer;

class Channel
{
public:
	Channel( ClickyServer &_server, ChannelIdT _id, LogWriter *_logWriter = NULL )
		: server(_server), id(_id), subtitlesAuthorId(0), channelLog( *this, _logWriter ) {};
	~Channel() {};

	void SetLogWriter( LogWriter *_logWriter = NULL ) { channelLog.SetLogWriter( _logWriter ); };

	/** Tell this channel somebody just clicked this subtitle.
	 * @returns True if the subtitle should be propagated, false otherwise.
	 */
	bool ClickSub( const UdpPacketShow &pkt, ServerConPtr _author, int believedLastSubNum );

	/** Tell this channel somebody just changed the autoplayer state.
	 * @returns True if the new state if the autoplayer should be propagated.
	 */
	bool SetPlayerState( const UdpPacketPlay &pkt, ServerConPtr _author );

	void LogShowAck( const UdpPacketShow &pkt, ServerConPtr _author, const ClickyTime &when );

	void LogLogin( ServerConPtr who, const ClickyTime &when ) { channelLog.LogLogin( who, when ); };
	void LogLogout( ServerConPtr who, const ClickyTime &when, NodeLogoutReasonT reason )  { channelLog.LogLogout( who, when, reason ); };
	void LogSetSubs( ServerConPtr who, const ClickyTime &when, size_t size ) { channelLog.LogSetSubs( who, when, size ); };
	void LogAckSubs( ServerConPtr who, const ClickyTime &when, size_t size ) { channelLog.LogAckSubs( who, when, size ); };

	const ChanSub &GetShownSub() { return shownSub; };
	const ChanPlayerState &GetPlayerState() { return playerState; };

	void SetSubtitles( const std::string &subs, NodeConIdT _authorId );
	const std::string &GetSubtitles( bool allow_srt ) { return (allow_srt && (srt_subtitles.size() > 0) ) ? srt_subtitles : plain_subtitles; };
	NodeConIdT GetSubtitlesAuthorId() { return subtitlesAuthorId; };
	bool AreSubtitlesLoaded() { return (srt_subtitles.size() > 0)||(plain_subtitles.size() > 0); };
	bool AreSubtitlesTimed() { return srt_subtitles.size() > 0; };

	ChannelIdT	GetId() { return id; };
	ChannelLog &GetLog() { return channelLog; };
	ClickyServer &GetServer() { return server; };

protected:
	ChannelIdT	id;

	std::string	srt_subtitles;
	std::string	plain_subtitles;
	NodeConIdT subtitlesAuthorId;

	ChanSub				shownSub;
	ChanPlayerState	playerState;

	ChannelLog 		channelLog;
	ClickyServer	&server;
};

#endif //_Channel_h
