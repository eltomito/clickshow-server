/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Channel_Log_h
#define _Channel_Log_h 1

#include "shared/src/str_utils.h"
#include "log_writer.h"
#include "server_con.h"
#include "log_guest_map.h"

//=========== class ChannelLogWriter ===========

class Channel;

class ChannelLogWriter : public LogWriter
{
public:
	ChannelLogWriter( Channel &_channel,
							const std::string &_dir = ".",
							const std::string &_fileBase = "clicklog",
							size_t _maxSize = DEFAULT_MAX_LOG_SIZE )
	: LogWriter( _dir, _fileBase, _maxSize ), channel( _channel ) {};
	~ChannelLogWriter() {};

protected:
	void onNewLogFile( const ClickyTime &logTime, std::string &dst );

protected:
	Channel	&channel;
};

//======== class LogClickInfo ===============

/** This class stores information about a click that will be stored in the log.
 * It stores the time it took for a subtitle click to go from its origin to a particular client.
 */
class LogClickInfo
{
public:
	/**
	 * @param _conId - the Id of the connection (client) this object refers to.
	 * @_delayMs - the number of milliseconds it took for the click to reach this connection.
	 * Note that the delay can be negative because the clocks on different machines
	 * aren't necessarily synchronized and the time adjustment maintained by clicky clients
	 * has an error margin equal to the round trip of a packet between the client and the server.
	 */
	LogClickInfo( NodeConIdT _conId = NODE_CON_ID_INVALID, long _delayMs = 0 ): conId(_conId), delayMs(_delayMs) {};

	~LogClickInfo() {};

	NodeConIdT	GetConId() const { return conId; };
	long 			GetDelay() const { return delayMs; };

protected:
	NodeConIdT	conId;
	long			delayMs;
};

class LCIArray
{
protected:
	typedef std::vector<LogClickInfo> arrayT;

	arrayT	lcis;
	size_t	actualSize;

	size_t	growBy;

	long dMin;
	long dMax;
	double dAvg;

public:
	LCIArray( size_t _reserveSize = 50, size_t _growBy = 50 ) : lcis( _reserveSize ), growBy( _growBy ) { clear(); };
	~LCIArray() {};

	void clear();
	void push_back( const LogClickInfo &lci );
	size_t size() { return actualSize; };
	std::string ToString( const std::string &sep = "," );
	void ToLogString( std::string &dst, bool includeStats = false );
};

class LogClickSub
{
protected:
	int				subNum;
	std::string		subText;
	NodeConIdT		clickerId;
	ClickyTime		clickTime;
	ChannelIdT		chanId;

public:
	static const int MAX_SUB_TEXT_LEN = 30;

	LogClickSub( ChannelIdT _chanId = 0, size_t defNumClickers = 5, size_t defNumShowers = 10 )
	:	clickers( defNumClickers, defNumClickers ),
		showers( defNumShowers, defNumShowers ),
		chanId( _chanId )
	{
		Clear();
	};

	void SetChanId( ChannelIdT _chanId ) { chanId = _chanId; };
	int GetSubNum() { return subNum; };
	const ClickyTime &GetClickTime() { return clickTime; };
	void Init( int _subNum = -1, const std::string &_subText = "", NodeConIdT _clickerId = 0, const ClickyTime &_cTime = ClickyTime(false) );
	void Clear() { Init(); };
	void NewClick( int _subNum, const std::string &_subText, ServerConPtr _con, const ClickyTime &_cTime )
	{
		Init( _subNum, _subText, _con->GetId(), _cTime );
	};

	void AddClick( const ServerConPtr _con, const ClickyTime &_cTime, bool click );
	void ToLogString( std::string &dst );

protected:

	void cleanString( std::string &str );

protected:
	LCIArray			clickers;
	LCIArray			showers;
};

class Channel;
class ChanSub;

class ChannelLog
{
protected:
	Channel 		&channel;
	LogClickSub	curSub;
	LogWriter	*logWriter;
	LogGuestMap guestMap;

	typedef unsigned int LogEntryType;
	static const LogEntryType LOG_LOGIN = 0;
	static const LogEntryType LOG_LOGOUT = 1;
	static const LogEntryType LOG_ACTIVE = 2;
	static const LogEntryType LOG_TYPES_COUNT = 3;

public:
	ChannelLog( Channel &_channel, LogWriter *_logWriter = NULL );
	~ChannelLog()
	{
		//flushCurSub();
	};

	void SetLogWriter( LogWriter *_logWriter = NULL ) { logWriter = _logWriter; };

	void LogClick( ServerConPtr who, const ClickyTime &when, const ChanSub &sub );
	void LogShowAck( ServerConPtr who, const ClickyTime &when, const ChanSub &sub );
	void LogLogin( ServerConPtr who, const ClickyTime &when ) { guestMap.Add( who ); logLoginOut( who, when, LOG_LOGIN ); };
	void LogLogout( ServerConPtr who, const ClickyTime &when, NodeLogoutReasonT reason ) { guestMap.Rem( who->GetId() ); logLoginOut( who, when, LOG_LOGOUT, reason ); };
	void LogSetSubs( ServerConPtr who, const ClickyTime &when, size_t size );
	void LogAckSubs( ServerConPtr who, const ClickyTime &when, size_t size );

	void FormatActive( std::string &dst, ServerConPtr who, const ClickyTime &when );
	LogGuestMap &GetGuestMap() { return guestMap; };

protected:
	void formatLoginOut( std::string &dst, ServerConPtr who, LogEntryType _type, NodeLogoutReasonT reason = WHY_LOGOUT_UNKNOWN );
	void logLoginOut( ServerConPtr who, const ClickyTime &when, LogEntryType _type, NodeLogoutReasonT reason = WHY_LOGOUT_UNKNOWN );
	void flushCurSub();
	void writeLogFile( const ClickyTime &ct, const std::string msg );

	static const char LogEntryLetters[ LOG_TYPES_COUNT + 1];
};

#endif //_Channel_Log_h
