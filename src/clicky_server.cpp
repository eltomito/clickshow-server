/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_server.h"

//====================== class ServerConfig =======================

#include "protocol_features.h"
#include "shared/src/default_log.h"
#include <fstream>

bool ServerConfig::Read( const std::string &fileName )
{
	std::ifstream ifs( fileName.c_str(), std::ifstream::in);
	if( !ifs ) {
		LOG_NET_ERROR("FAILED to open config file \"%s\"\n", fileName.c_str() );
		return false;
	}

	Json::Value	root;
	try {
		ifs >> root;
	} catch(...) {
		LOG_NET_ERROR("Json error in the \"%s\" config file!\n", fileName.c_str() );
		return false;
	}

	if( root.isNull() ) {
		LOG_NET_ERROR("Config error: The file isn't JSON!\n");
		return false;
	}
	if( !readUsers( root["users"] ) ) {
		LOG_NET_ERROR("Failed to read the \"users\" array!\n");
		return false;
	}
	if( root["logFileName"].isString() ) {
		logFilePath = root["logFileName"].asString();
	}

	if( root["logFileSize"].isInt() ) {
		logFileSize = root["logFileSize"].asInt();
	}
	return true;
}

bool ServerConfig::readUsers( Json::Value &users )
{
	if( users.isNull() || !users.isArray() ) { return false; }

	Json::Value *valUser;
	NodeUser *u;
	std::string name;
	std::string pwd;
	UserPermsT	perms;
	int cnt = 0;

	for( int i = 0; i < users.size(); ++i ) {
		valUser = &users[i];
		if( valUser->isNull() || !valUser->isObject() ) {
			LOG_NET_ERROR("Ignoring bad value in user array at index %d.\n", i );
		} else {
			perms = 0;
			name.clear();
			pwd.clear();
			if( !(*valUser)["name"].isNull() ) { name = (*valUser)["name"].asString(); }
			if( !(*valUser)["pwd"].isNull() ) { pwd = (*valUser)["pwd"].asString(); }
			if( !(*valUser)["perms"].isNull() && (*valUser)["perms"].isUInt() ) { perms = (UserPermsT)( (*valUser)["perms"].asUInt() ); }
			if( !name.empty() && !pwd.empty() ) {
				u = server.users.AddUser( name, pwd, perms );
				if( u ) {
					LOG_NET_INFO("Added config user %d at index %d: %s\n", i, cnt, u->ToString(", ").c_str() );
					++cnt;
				} else {
					LOG_NET_ERROR("Bad user entry at index %d: name: \"%s\", pwd: \"%s\".\n", i, name.c_str(), pwd.c_str() );
				}
			}
		}
	}
	return ( cnt > 0 );
}

void ServerConfig::setDefaults()
{
	logFilePath = "clicklog";
	logFileSize = 100000;
}

//=============== class ShowSubSender =================

class ShowSubSender
{
public:
	ShowSubSender( ClickyServer &_server, Channel &ch ) : server(_server)
	{
		sub = ch.GetShownSub();
		chanId = ch.GetId();
		playerIsPlaying = ch.GetPlayerState().isPlaying;
	};

	NCPForEachF operator()(ServerConPtr cl)
	{
		//only send SHOW messages to those who aren't their author and are on the right channel
		if(( sub.clickerId != cl->GetId() )&&( chanId == cl->GetChannel() )) {
			//they must be either unable to play timed subs or the player must not be playing
			if( !PrF::canAutoplayTimedSubs( *cl ) || !playerIsPlaying ) {
				cl->SendShow( sub.subNum, sub.subText, &sub.clickTime );
			}
		}
		return NCPForEachF::NCP_CONTINUE;
	};

	static NCPForEachF CallBound( ServerConPtr cl, void *obj ) {
		return ( *((ShowSubSender*)obj) )( cl );
	};
protected:
	ClickyServer	&server;
	ChanSub			sub;
	ChannelIdT		chanId;
	bool				playerIsPlaying;
};

//=============== class PlayerStateSender =================

class PlayerStateSender
{
public:
	PlayerStateSender( ClickyServer &_server, Channel &ch ) : server(_server)
	{
		ps = ch.GetPlayerState();
		chanId = ch.GetId();
	};

	NCPForEachF operator()(ServerConPtr cl)
	{
		if(( ps.GetAuthorId() != cl->GetId() )&&( chanId == cl->GetChannel() )
			&& PrF::canAutoplayTimedSubs( *cl ) ) {
			cl->SendPlay( ps );
		}
		return NCPForEachF::NCP_CONTINUE;
	};

	static NCPForEachF CallBound( ServerConPtr cl, void *obj ) {
		return ( *((PlayerStateSender*)obj) )( cl );
	};
protected:
	ClickyServer			&server;
	ChanPlayerState		ps;
	ChannelIdT				chanId;
};

//=============== class ClickyServer =================

void ClickyServer::LogShowAck( const UdpPacketShow &pkt, const ServerConPtr con, const ClickyTime &when )
{
	Channel *ch = channels.GetChannel( con->GetChannel() );
	if( ch == NULL ) {
		LOG_NET_ERROR("This con wants to click subs on an invalid channel: %s\n", con->ToString(",").c_str() );
		return;
	};
	ch->LogShowAck( pkt, con, when );
}

void ClickyServer::LogSendSubsAck( const ServerConPtr con, size_t size )
{
	Channel *ch = channels.GetChannel( con->GetChannel() );
	if( ch == NULL ) {
		LOG_NET_ERROR("This con wants to ack new subs on an invalid channel: %s\n", con->ToString(",").c_str() );
		return;
	};
	ClickyTime nowTime( true );
	ch->LogAckSubs( con, nowTime, size );
}

/** Let the server know that some client clicked a subtitle.
 */
void ClickyServer::ClickSubtitle( const UdpPacketShow &insub, ServerConPtr author, int believedLastSubNum )
{
	Channel *ch = channels.GetChannel( author->GetChannel() );
	if( ch == NULL ) {
		LOG_NET_ERROR("This con wants to click subs on an invalid channel: %s\n", author->ToString(",").c_str() );
		return;
	};
	if( ch->ClickSub( insub, author, believedLastSubNum ) ) {
		SendShowSubtitle( *ch );
		if( ch->AreSubtitlesTimed() ) {
			SendPlayerState( *ch );
		}
	}
}

/** Let the server know that someone is trying to change the player state.
 */
void ClickyServer::SetPlayerState( const UdpPacketPlay &pkt, ServerConPtr author )
{
	if( PrF::canAutoplayTimedSubs( *author ) ) {
		Channel *ch = channels.GetChannel( author->GetChannel() );
		if( ch == NULL ) {
			LOG_NET_ERROR("This con wants to set player state on an invalid channel: %s\n", author->ToString(",").c_str() );
			return;
		}
		if( !ch->AreSubtitlesTimed() ) {
			LOG_NET_ERROR("Con trying to set player state on a channel without timed subs! %s\n", author->ToString(",").c_str() );
			return;
		}
		if( ch->SetPlayerState( pkt, author ) ) {
			SendPlayerState( *ch );
		}
	}
}

/** Tell all clients connected to this channel to show the current subtitle.
 */
void ClickyServer::SendShowSubtitle( Channel &ch )
{
	ShowSubSender showSender( *this, ch );
	cons.ForEach( ShowSubSender::CallBound, (void*)&showSender );
}

/** Tell all clients connected to this channel who understand autoplay to update their player state
 */
void ClickyServer::SendPlayerState( Channel &ch )
{
	PlayerStateSender playerStateSender( *this, ch );
	cons.ForEach( PlayerStateSender::CallBound, (void*)&playerStateSender );
}

class SetSubtitlesSender
{
public:
	SetSubtitlesSender( ClickyServer &_server, Channel &ch ) : server(_server)
	{
		chanId = ch.GetId();
		plain_subs = &ch.GetSubtitles( false );	//these are definitely plain subtitles
		srt_subs = &ch.GetSubtitles( true );		//these *may* be srt subtitles (if available). Otherwise, they're the same as the plain_subs.
		authorId = ch.GetSubtitlesAuthorId();
	};
	NCPForEachF operator()(ServerConPtr cl)
	{
		if(( cl->GetChannel() == chanId )&&( cl->GetId() != authorId )) {
			cl->SendSubtitles( PrF::acceptsTimedSubs( *cl ) ? *srt_subs : *plain_subs );
		}
		return NCPForEachF::NCP_CONTINUE;
	};
	static NCPForEachF CallBound( ServerConPtr cl, void *obj ) {
		return ( *((SetSubtitlesSender*)obj) )( cl );
	};
protected:
	ClickyServer		&server;
	const std::string	*srt_subs;
	const std::string	*plain_subs;
	ChannelIdT			chanId;
//	ServerConPtr		author;
	NodeConIdT		authorId;
};

void ClickyServer::SetSubtitles( const std::string &subs, ServerConPtr author )
{
	if( !author ) {
		LOG_NET_ERROR("You can't SetSubtitles() from a NULL author!" );
		return;
	}
	Channel *ch = channels.GetChannel( author->GetChannel() );
	if( ch == NULL ) {
		LOG_NET_ERROR("This con (%s) wants to set subs on an invalid channel %u \n", author->ToString(",").c_str(), (unsigned int)author->GetChannel() );
		return;
	};
	ch->SetSubtitles( subs, author->GetId() );
	ClickyTime nowTime( true );
	ch->LogSetSubs( author, nowTime, subs.size() + 1 );

	SendSetSubtitles( *ch );
}

void ClickyServer::SendSetSubtitles( Channel &ch )
{
	SetSubtitlesSender sss( *this, ch );
	cons.ForEach( SetSubtitlesSender::CallBound, (void*)&sss );
}

class ByeSender
{
public:
	ByeSender() {};
	NCPForEachF operator()(ServerConPtr cl)
	{
		cl->Disconnect();
		return NCPForEachF::NCP_CONTINUE;
	};
	static NCPForEachF CallBound( ServerConPtr cl, void *obj ) {
		return ( *((ByeSender*)obj) )( cl );
	};
};

void ClickyServer::LogoutAll()
{
	ByeSender byeSender;
	cons.ForEach( ByeSender::CallBound, (void*)&byeSender );
}

/** Things to do after somebody successfully logs in.
 */
void ClickyServer::onLogin( ServerConPtr cl )
{
	LOG_NET_INFO("Welcome, newcomer! Login:\n%s\n", cl->ToString(",").c_str() );
	cl->SetChannel(0);
	Channel *ch = channels.GetChannel( cl->GetChannel() );
	if( ch == NULL ) {
		LOG_NET_ERROR("This is so bad! We've just assigned a newcomer to a non-existing channel!\n%s\n", cl->ToString(",").c_str() );
		return;
	};

	ClickyTime nowTime( true );
	ch->LogLogin( cl, nowTime );

	if( ch->AreSubtitlesLoaded() ) {
		LOG_NET_INFO("Sending subtitles (size=%li) to connection %li...\n", (long)ch->GetSubtitles(PrF::acceptsTimedSubs( *cl )).size(), (long)cl->GetId() );
		cl->SendSubtitles( ch->GetSubtitles(PrF::acceptsTimedSubs( *cl )) );
	} else {
		LOG_NET_INFO("No subtitles to send to connection %li...\n", (long)cl->GetId() );
		ChanSub sub = ch->GetShownSub();
		cl->SendShow( sub.subNum, sub.subText, &nowTime );	//&sub.clickTime
	}
}

void ClickyServer::onLogout( ServerConPtr cl, NodeLogoutReasonT why )
{
	Channel *ch = channels.GetChannel( cl->GetChannel() );
	if( ch == NULL ) {
		LOG_NET_INFO("Logging out a connection that has no channel.\n%s\n", cl->ToString(",").c_str() );
		return;
	};
	ch->LogLogout( cl, ClickyTime(true), why );
}
