/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Server_Con_h
#define _Server_Con_h 1

#include "shared/src/net/node/node_con.h"
#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/con_var_msg.h"
#include "convars/server_var_show.h"
#include "convars/server_var_hi_bye.h"
#include "convars/server_var_perms.h"
#include "convars/server_var_file_in.h"
#include "convars/server_var_file_out.h"
#include "convars/server_var_play.h"
#include "shared/src/net/node/convars/con_var_file.h"
#include "shared/src/player/player_state.h"

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/weak_ptr.hpp>

using boost::asio::ip::udp;

class ServerCon : public NodeCon
{
public:
	ServerCon( const udp::endpoint &_endpoint, void *_server, NodeConIdT _id, ChannelIdT _chan = 0 )
		: NodeCon( _endpoint, _server, _id ),
		channel( _chan ),
		varMsg( *this ),
		varShow( *this, DEFAULT_RESEND_TIME ),
		varHiBye( *this, DEFAULT_RESEND_TIME ),
		varPerms( *this, DEFAULT_RESEND_TIME ),
	   varSubsIn( *this, FILE_ROLE_SUBTITLES, DEFAULT_FILE_RESEND_TIME ),
	   varSubsOut( *this, FILE_ROLE_SUBTITLES, DEFAULT_FILE_RESEND_TIME ),
	   varSubs( *this, &this->varSubsIn, &this->varSubsOut ),
	   varPlay( *this, DEFAULT_RESEND_TIME )
	{
		LOG_NET_DETAIL("Constructed a new ServerCon! id=%lu\n", (unsigned long)_id );
//		LOG_NET_DETAIL("ServerCon this=%lu, varHiBye.con = %lu\n", (unsigned long)this, (unsigned long)&varHiBye.GetCon() );
	};

	ServerCon()
	:	varMsg( *this ),
		channel(0),
		varShow( *this, DEFAULT_RESEND_TIME ),
		varHiBye( *this, DEFAULT_RESEND_TIME ),
		varPerms( *this, DEFAULT_RESEND_TIME ),
	   varSubsIn( *this, FILE_ROLE_SUBTITLES, DEFAULT_FILE_RESEND_TIME ),
	   varSubsOut( *this, FILE_ROLE_SUBTITLES, DEFAULT_FILE_RESEND_TIME ),
	   varSubs( *this, &this->varSubsIn, &this->varSubsOut ),
	   varPlay( *this, DEFAULT_RESEND_TIME )
	{};

	ServerCon( const ServerCon &other )
		: NodeCon( other ),
		channel( other.channel ),
		varMsg( *this ),
		varShow( *this, other.varShow ),
		varHiBye( *this, other.varHiBye ),
		varPerms( *this, DEFAULT_RESEND_TIME ),
	   varSubsIn( *this, FILE_ROLE_SUBTITLES, DEFAULT_RESEND_TIME ),
	   varSubsOut( *this, FILE_ROLE_SUBTITLES, DEFAULT_RESEND_TIME ),
	   varSubs( *this, &this->varSubsIn, &this->varSubsOut ),
	   varPlay( *this, DEFAULT_RESEND_TIME )
	{};

	~ServerCon() {};

	void SetWeakSelf( boost::weak_ptr<ServerCon> _me ) { weakMe = _me; };
	boost::shared_ptr<ServerCon> GetSharedSelf() { return weakMe.lock(); };

	void			SetChannel( ChannelIdT _chan ) { channel = _chan; };
	ChannelIdT	GetChannel() const { return channel; };

	bool Disconnect();

	bool Send( UdpPacket &pkt, bool setSentTime = true );
	void SendShow( int num, const std::string &text, const ClickyTime *clickTime = NULL ) { varShow.Send( num, text, clickTime ); };
	void SendPlay( const PlayerState &ps ) { varPlay.Send( ps ); };

	void SendBye() { varHiBye.SendBye(); };
	void SendMsg( NetMsgT code ) { varMsg.Send( code ); };
	void SendPerms( UserPermsT perms ) { varPerms.Send( perms ); };
	void SendSubtitles( const std::string &subs ) { varSubsOut.SendString( subs ); };

	void UpdateShownSubtitle();

	void handleHi( UdpPacketHi &pkt ) {
		varHiBye.Receive( pkt );
	};
	void handleBye( UdpPacketBye &pkt ) { varHiBye.Receive( pkt ); };
	void handleShow( UdpPacketShow &pkt ) { varShow.Receive( pkt ); };
	void handlePlay( UdpPacketPlay &pkt ) { varPlay.Receive( pkt ); };
	void handlePerms( UdpPacketPerms &pkt ) { varPerms.Receive( pkt ); };
	void handleFileHeader( UdpPacketFileHeader &pkt ) { varSubs.ReceiveHeader( pkt ); };
	void handleFileBody( UdpPacketFileBody &pkt ) { varSubs.ReceiveBody( pkt ); };

	void handleChat( UdpPacketChat &pkt ) {};
	void handleMsg( UdpPacketChat &pkt ) { varMsg.Receive( pkt ); };

	void handleInvalid( UdpPacket &pkt ) {};

	void checkIntegrity( int n );

	//this function is called at regular intervals
	void doChores( const ClickyTime &nowTime )
	{
		varHiBye.DoChores( nowTime );
		if( IsConnected() ) {
			varShow.DoChores( nowTime );
			varMsg.DoChores( nowTime );
			varPerms.DoChores( nowTime );
			varSubs.DoChores( nowTime );
		}
	};

	std::string ToString( const std::string &separator = "\n", int verbosity = 1 ) const;

	ChannelIdT		channel;

	ConVarMsg		varMsg;
	ServerVarShow	varShow;
	ServerVarHiBye	varHiBye;
	ServerVarPerms	varPerms;
	ServerVarPlay	varPlay;

	ServerVarFileIn		varSubsIn;
	ServerVarFileOut		varSubsOut;
	ConVarFile				varSubs;

	boost::weak_ptr<ServerCon> weakMe;
};

typedef boost::shared_ptr<ServerCon> ServerConPtr;

#endif //_Server_Con_h
