/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "channel.h"
#include "shared/src/subtitle_formats/utils/srt_stripper.h"

void ChanSub::Set( const UdpPacketShow &pkt, NodeConIdT _authorId )
{
	if( pkt.IsHide() ) {
		subNum = -2;
		subText = "";
	} else {
		subNum = pkt.subNum;
		subText = pkt.subText;
	}
	clickerId = _authorId;
	clickTime = pkt.GetSentTime();
}

void ChanPlayerState::Set( const UdpPacketPlay &pkt, NodeConIdT _authorId )
{
	startTimestamp = pkt.startTimestamp;
	startTimecode = pkt.startTimecode;
	delay = pkt.delay;
	speed = pkt.speed;
	isPlaying = pkt.isPlaying;
	shownSub = pkt.shownSub;
	nextSub = pkt.nextSub;
	authorId = _authorId;
	changeTime.SetToNow();
}

void ChanPlayerState::SetSubNum( int _shownSub )
{
	shownSub = _shownSub;
	if( shownSub >= 0 ) {
		nextSub = shownSub + 1;
	}
}

bool Channel::SetPlayerState( const UdpPacketPlay &pkt, ServerConPtr _author )
{
	if( pkt.GetSentTime() <= playerState.GetChangeTime() ) {
		return false;	//an old packet - ignore it
	}
	playerState.Set( pkt, _author->GetId() );
	return true;
}

/** Tell this channel somebody just clicked this subtitle.
 * @returns True if the subtitle should be propagated, false otherwise.
 */
bool Channel::ClickSub( const UdpPacketShow &pkt, ServerConPtr _author, int believedLastSubNum )
{
	bool retval = true;

	if( pkt.GetSentTime() <= shownSub.clickTime ) {
		return false;	//an old packet - ignore it
	}

	if( pkt.IsHide() && shownSub.subNum != believedLastSubNum ) {
		return false; //this clicker is trying to hide a subtitle that isn't being shown.
	}

	if( pkt.subNum == shownSub.subNum ) {
		//the same subtitle clicked again
		if( _author->GetId() == shownSub.GetClickerId() ) {
			//...by the same author! That's probably an error in the client code.
			LOG_NET_WARN("This connection id %d clicked the same subtitle #%d twice!\n", _author->GetId(), shownSub.subNum );
			return false;
		}
		//...by someone else. Tell the caller to ignore it.
		retval = false;
	} else {
		shownSub.Set( pkt, _author->GetId() );
		playerState.SetSubNum( shownSub.subNum );
		retval = true;
	}

	channelLog.LogClick( _author, pkt.GetSentTime(), shownSub );

	return retval;
}

void Channel::LogShowAck( const UdpPacketShow &pkt, ServerConPtr _author, const ClickyTime &when )
{
	if( !pkt.IsAck() ) {
		LOG_FLG_WARN("Somebody wants me to log show ack from a non ack packet: %s\n", pkt.ToString(",").c_str() );
		return;
	}
	if( pkt.subNum != shownSub.subNum ) {
		LOG_FLG_DETAIL("Asked to log a show ack for sub #%d but my current subtitle is #%d. Sorry.\n", pkt.subNum, shownSub.subNum );
		return;
	};
	channelLog.LogShowAck( _author, when, shownSub );
}

void Channel::SetSubtitles( const std::string &subs, NodeConIdT _authorId )
{
	subtitlesAuthorId = _authorId;
	if( SrtStripper::IsSrt( subs ) ) {
		srt_subtitles = subs;
		plain_subtitles.clear();
		if( !SrtStripper::SrtToPlain( srt_subtitles, &plain_subtitles, false ) ) {
			LOG_NET_WARN("WEIRD! SrtToPlain() should never fail in non-strict mode!!! (srt size = %i)\n", srt_subtitles.size() );
		}
	} else {
		plain_subtitles = subs;
		srt_subtitles.clear();
	}
	shownSub.Reset( _authorId );
}
