/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "server_con.h"
#include "clicky_server.h"

bool ServerCon::Send( UdpPacket &pkt, bool setSentTime )
{
	if( node == NULL ) {
		LOG_NET_ERROR("Tried to send a packet to a ServerCon but its node is NULL!\n");
		return false;
	}
	return ((ClickyServer*)node)->Send(this, pkt, setSentTime );
};

bool ServerCon::Disconnect()
{
	switch( connState ) {
		case DISCONNECTED:
			LOG_NET_ERROR("Cannot disconnect! I'm not connected!\n");
			return false;
		break;
		case CONNECTED:
			SetConnected( DISCONNECTING );
			SendBye();
		break;
		case CONNECTING:
			SetConnected( DISCONNECTED );
			SendBye();
		break;
		case DISCONNECTING:
			SetConnected( DISCONNECTED );
			Kill( WHY_LOGOUT_SERVER_FORCED );
		break;
		default:
			LOG_NET_ERROR("Weird! Asked to disconnect in an unknown state (%d)! I'll do it anyway...\n", (int)connState );
			SetConnected( DISCONNECTED );
		break;
	}
}

void ServerCon::UpdateShownSubtitle()
{
	Channel *ch = ((ClickyServer*)node)->GetChannelById( GetChannel() );
	if( ch == NULL ) {
		LOG_NET_ERROR("This con is on a non-existing channel! %s\n", ToString(",").c_str() );
		return;
	};
	const ChanSub &sub = ch->GetShownSub();
	SendShow( sub.subNum, sub.subText, &sub.clickTime );
}

std::string ServerCon::ToString( const std::string &separator, int verbosity ) const
{
	std::string dst;
	dst = NodeCon::ToString( separator, verbosity );
	StrUtils::Printf( dst, "%schan=%ld", separator.c_str(), (long)channel );
	if( verbosity >= 1 ) {
		StrUtils::Printf( dst, "%sSubs Out: %s%s",
			separator.c_str(),
			varSubsOut.ToString( ",", verbosity ).c_str(),
			separator.c_str() );
		StrUtils::Printf( dst, "Subs In: %s%s",
			varSubsIn.ToString( ",", verbosity ).c_str(),
			separator.c_str() );
	}
	return dst;
}

void ServerCon::checkIntegrity( int n )
{
	if( &varHiBye.GetCon() != (NodeCon*)this ) {
		LOG_NET_ERROR("Check %i: varHiBye is pointing to a wrong connection! (%lu)\n", n, &varHiBye.GetCon() );
	}
}
