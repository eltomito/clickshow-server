/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "channel_log.h"
#include "channel.h"
#include "clicky_server.h"

// ================ class LCIArray =======================

void LCIArray::clear()
{
	actualSize = 0;
	dMin = 0;
	dAvg = 0;
	dMax = 0;
}

void LCIArray::push_back( const LogClickInfo &lci )
{
	if( actualSize >= lcis.size() ) { lcis.insert( lcis.end(), growBy, LogClickInfo() ); }
	if( actualSize >= lcis.size() ) {
		LOG_FLG_ERROR("Failed to allocate %d new LogClickInfos!\n", growBy );
		return;
	}
	lcis[actualSize] = lci;
	++actualSize;

	long d = lci.GetDelay();
	if( d < dMin ) { dMin = d; }
	if( d > dMax ) { dMax = d; }
	dAvg += ( (double)d - dAvg ) / (double)actualSize;
};

std::string LCIArray::ToString( const std::string &sep )
{
	 std::string dst;
	 dst.append("LCIArray: ");
	 ToLogString( dst );
	 return dst;
};

void LCIArray::ToLogString( std::string &dst, bool includeStats )
{
	if( includeStats ) {
		StrUtils::Printf( dst, "[%u:%ld, %ld, %ld]", (unsigned)actualSize, (long)dMin, (long)dAvg, (long)dMax );
		if( actualSize > 0 ) { dst.push_back(' '); }
	}
	for( int i = 0; i < actualSize; i++ ) {
		StrUtils::Printf( dst, "%u:%ld,", (unsigned)lcis[i].GetConId(), (long)lcis[i].GetDelay() );
	}
	if( actualSize > 0 ) {
		dst.erase( dst.size() - 1, 1 ); //delete the trailing comma
	}
};

//====================== class LogClickSub ======================

void LogClickSub::Init( int _subNum, const std::string &_subText, NodeConIdT _clickerId, const ClickyTime &_cTime )
{
	subNum = _subNum;

	subText = StrUtils::TruncUTF8String( _subText, MAX_SUB_TEXT_LEN );
	//subText = _subText;
	cleanString( subText );

	clickerId = _clickerId;
	clickTime = _cTime;
	clickers.clear();
	showers.clear();
}

void LogClickSub::AddClick( ServerConPtr _con, const ClickyTime &_cTime, bool click )
{
	long delay = (long)( _cTime - clickTime );
	LogClickInfo lci( _con->GetId(), delay );
	if( click ) {
		clickers.push_back( lci );
	} else {
		showers.push_back( lci );
	}
}

void LogClickSub::ToLogString( std::string &dst )
{
	if( subNum >= 0 ) {
		StrUtils::Printf( dst, "C %u#%d \"%s\" %u", (unsigned)chanId, subNum, subText.c_str(), (unsigned)clickerId );
	} else {
		StrUtils::Printf( dst, "C %u#- %u", (unsigned)chanId, (unsigned)clickerId );
	}
	if( clickers.size() > 0 ) {
		dst.push_back(' ');
		clickers.ToLogString( dst, false );
	}
	if( showers.size() > 0 ) {
		dst.append( " S " );
		showers.ToLogString( dst, true );
	}
}

/** Replaces all control characters and double quotes with spaces.
 * Note that it works well wil UTF-8, because all bytes in a multi-byte character are > 128.
 */
void LogClickSub::cleanString( std::string &str )
{
	size_t len = str.size();
	for( size_t i = 0; i < len; ++i ) {
		if( ( ((unsigned char)str[i]) < 32 ) || ( str[i] == '"' ) ) {
			str[i] = ' ';
		}
	}
}

//=========== class ActiveConLogger ============

class ActiveConLogger
{
public:
	ActiveConLogger( ChannelLog &_channelLog, ChannelIdT _channelId, const ClickyTime &_logTime, std::string &_logString )
	:	channelLog(_channelLog),
		channelId(_channelId),
		logTime(_logTime),
		logString(_logString)
	{};
	~ActiveConLogger() {};

	NCPForEachF operator()(ServerConPtr cl)
	{
		if( cl->GetChannel() == channelId ) {
			channelLog.FormatActive( logString, cl, logTime );
		}
		return NCPForEachF::NCP_CONTINUE;
	};

	static NCPForEachF CallBound( ServerConPtr cl, void *obj ) {
		return ( *((ActiveConLogger*)obj) )( cl );
	};
protected:
	std::string			&logString;
	ChannelLog			&channelLog;
	ChannelIdT			channelId;
	const ClickyTime	&logTime;
};

// ================ class ChannelLogWriter =============
void ChannelLogWriter::onNewLogFile( const ClickyTime &logTime, std::string &dst )
{
	ActiveConLogger acl( channel.GetLog(), channel.GetId(), logTime, dst );
	channel.GetLog().GetGuestMap().ForEach( ActiveConLogger::CallBound, (void*)&acl );
}
//========================= class ChannelLog ====================

ChannelLog::ChannelLog( Channel &_channel, LogWriter *_logWriter )
	:	channel(_channel), logWriter(_logWriter)
{
	curSub.SetChanId( channel.GetId() );
}

void ChannelLog::LogClick( ServerConPtr who, const ClickyTime &when, const ChanSub &sub )
{
	if( sub.subNum != curSub.GetSubNum() ) {
		flushCurSub();
		curSub.NewClick( sub.subNum, sub.subText, who, when );
		return;
	}
	curSub.AddClick( who, when, true );
}

void ChannelLog::LogShowAck( ServerConPtr who, const ClickyTime &when, const ChanSub &sub )
{
	if( sub.subNum != curSub.GetSubNum() ) {
		LOG_FLG_DETAIL("Somebody tried to log  show ack for sub #%d but current sub is #%d\n", sub.subNum, curSub.GetSubNum() );
		return;
	}
	curSub.AddClick( who, when, false );
}

void ChannelLog::LogSetSubs( ServerConPtr who, const ClickyTime &when, size_t size )
{
	std::string dst;
	StrUtils::Printf( dst, "S %u %u %lu", (unsigned)channel.GetId(), (unsigned)who->GetId(), size );
	flushCurSub();
	writeLogFile( when, dst );
}

void ChannelLog::LogAckSubs( ServerConPtr who, const ClickyTime &when, size_t size )
{
	std::string dst;
	StrUtils::Printf( dst, "R %u %u %lu", (unsigned)channel.GetId(), (unsigned)who->GetId(), size );
	flushCurSub();
	writeLogFile( when, dst );
}

void ChannelLog::FormatActive( std::string &dst, ServerConPtr who, const ClickyTime &when )
{
	std::string tmp;
	formatLoginOut( tmp, who, LOG_ACTIVE );
	LogWriter::FormatLogLine( when, tmp, dst );
}

void ChannelLog::flushCurSub() {
	if( !curSub.GetClickTime().IsValid() ) { return; }

	std::string dst;
	curSub.ToLogString( dst );
	dst.push_back('\n');
	writeLogFile( curSub.GetClickTime(), dst );
	curSub.Init();
}

void ChannelLog::writeLogFile( const ClickyTime &ct, const std::string msg )
{
	if( logWriter != NULL ) {
		logWriter->Write( ct, msg );
		return;
	}
	LOG_FLG_INFO("%s %s\n", ct.ToDenseString( false, 0 ).c_str(), msg.c_str() );
}

void ChannelLog::logLoginOut( ServerConPtr who, const ClickyTime &when, LogEntryType _type, NodeLogoutReasonT reason )
{
	std::string dst;
	flushCurSub();
	formatLoginOut( dst, who, _type, reason );
	writeLogFile( when, dst );
}

void ChannelLog::formatLoginOut( std::string &dst, ServerConPtr who, LogEntryType _type, NodeLogoutReasonT reason )
{

	const char *userName = "?";
	UserPermsT	userPerms = 0;
	NodeUser *u = who->GetUser();
	if( u != NULL ) {
		userName = u->GetName().c_str();
		userPerms = u->GetPerms();
	}

	char typeLetter = _type >= LOG_TYPES_COUNT ? 'X' : LogEntryLetters[ _type ];
	StrUtils::Printf( dst, "%c %u %u@%s ~\"%s\" *0x%x v\"%s\"",
							typeLetter,
							(unsigned)channel.GetId(),
							(unsigned)who->GetId(),
							AsioUtils::EndpointToString( who->GetConstEndpoint() ).c_str(),
							userName,
							(unsigned long)userPerms,
							who->GetVenue().c_str()
						);
	if( ( _type == LOG_LOGOUT ) && reason != 0 ) {
		StrUtils::Printf( dst, " ?%d", (unsigned)reason );
	}
	dst.push_back('\n');
}

const char ChannelLog::LogEntryLetters[] = "IOA";
