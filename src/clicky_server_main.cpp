/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_server_debug.h"
#include "clicky_server.h"
#include "server_version.h"

#include <string>
#include <stdio.h>
#include <cstdlib>

#include "boost/asio.hpp"

void printHelp( const char *cmdName, unsigned short port )
{
		printf("\
Usage: %s [<port>]\n\
       %s --version|-V\n\
       \nThe default port is %d.\n", cmdName, cmdName, port );
}

int main( int argc, char *argv[] ) {

	char version_str[32];

	if( SERVER_VERSION_REVISION ) {
		sprintf( version_str, "%i.%i.%i", SERVER_VERSION_MAJOR, SERVER_VERSION_MINOR, SERVER_VERSION_REVISION );
	} else {
		sprintf( version_str, "%i.%i", SERVER_VERSION_MAJOR, SERVER_VERSION_MINOR );
	}

	unsigned short port = DEFAULT_SERVER_PORT;

	if( argc > 1 ) {
		if( argc == 2 ) {
			if(( 0 == strcmp( argv[1], "--version" ) )||( 0 == strcmp( argv[1], "-V" ) )) {
				printf("%s\n", version_str);
				exit(0);
			}
			long int p = strtol(argv[1], NULL, 10);
			if( p != 0 ) {
				port = p;
			} else {
				printf("Invalid port number: \"%s\".\n", argv[1]);
				printHelp(argv[0], port);
				return 1;
			}
		} else {
			printHelp(argv[0], port);
			return 1;
		}
	}

	AutoPacket::StaticInit();

	boost::asio::io_service ioService;
	std::string cfgFileName = argv[0];
	cfgFileName.append(".cfg");

	ClickyServer cs( ioService, cfgFileName, port );

	cs.Start();

	printf("ClickyServer %s started on port %d.\n", version_str, port);

   char inBuf[ 255 ];
   ssize_t inlen;
   bool alive = true;
	while( alive ) {
		inlen = read( 0, inBuf, sizeof(inBuf) );
		if(inlen > 0) {
			if( ( inlen > 1 )&&( inBuf[0] == '/' ) ) {
				switch( inBuf[1] )
				{
					case 'q':
						printf("Bye!\n");
						alive = false;
					break;
					case 'x':
						printf("Sending bye to all connections...!\n");
						cs.LogoutAll();
					break;
					case 'd':
						printf("%s\n", cs.ToString("\n").c_str() );
					break;
					default:
						printf("What is \"%c\" supposed to mean? Press enter for help.\n", inBuf[1] );
					break;
				}
			} else {
				printf("Commands:\n/d - Dump the state of this server on you!\n/x - Send BYE to all connections.\n/q - Quit.\n");
			}
		}
	};
	
	cs.Stop();

	AutoPacket::StaticCleanup();

	return 0;
}
