/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Log_Guest_Map_h
#define _Log_Guest_Map_h 1

#include <map>

#include "shared/src/net/node/node_con_pool.h"
#include "server_con.h"
#include "shared/src/semaph.h"
#include "shared/src/default_log.h"

class LogGuestMap : public Lockable
{
public:
	typedef typename std::map<NodeConIdT, ServerConPtr> mapT;
	typedef typename mapT::iterator mapIt;
	typedef typename mapT::value_type mapValT;

	LogGuestMap() {};
	~LogGuestMap() {};

	void Add( ServerConPtr who );
	void Rem( NodeConIdT whoId );
	void ForEach( NCPForEachF (&doWhat)(ServerConPtr cl, void *_data), void *data );

private:
	mapT guestMap;
};

#endif //_Log_Guest_Map_h
