/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Protocol_Features_h_
#define _Protocol_Features_h_ 1

#include "server_con.h"

class PrF
{
public:
	static bool acceptsTimedSubs( int protoVer ) { return protoVer >= 2; };
	static bool canAutoplayTimedSubs( int protoVer ) { return protoVer >= 2; };

	static bool acceptsTimedSubs( const ServerCon &con ) { return acceptsTimedSubs(con.GetProtocolVersion()); };
	static bool canAutoplayTimedSubs( const ServerCon &con ) { return canAutoplayTimedSubs(con.GetProtocolVersion()); };

protected:
	PrF() {};
	~PrF() {};
};

#endif // _Protocol_Features_h_
